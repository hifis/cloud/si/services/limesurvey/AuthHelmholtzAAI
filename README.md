# LimeSurvey custom authentication plugin for Helmholtz AAI

AuthHelmholtzAAI is an OpenID Connect authentication plugin for LimeSurvey, which was developed for integration into the [Helmholtz.Cloud](https://helmholtz.cloud/) via Helmholtz AAI.

## Features

- Authorization is based on the eduPerson attribute scheme:
  - eduPersonEntitlement (group) as per the [AARC-G002](https://aarc-community.org/guidelines/aarc-g002) recommendation.
- It supports multi-tenancy by creating local user groups in LimeSurvey from the group information derived from the eduPersonEntitlement attribute.
- The local user profile (user display name and email address) is created according to the attribute information of the given user signing in.
- Unauthorized users attempting to log in can be moved automatically to a dedicated user group ("soft deprovisioning").
- User auto-creation and set default permissions to create surveys.
- LimeSurvey Remote-API (LSRC2) authorization using an OIDC access token.
- OAuth token revocation on user logout.
- Automatic setting the user permissions for survey themes that are prefixed with the same string as the group name.

## Prerequisites

- For the plugin to work as intended it is important *GROUP MEMBER CAN ONLY SEE OWN GROUP* is set to ON. To do this, please go to:
  - Configuration > Global settings > Security (LimeSurvey Version 3.x)
  - Configuration > Global > Security (LimeSurvey Version 5.x and above)

## Limitations

- As LimeSurvey currently does not support user group names of more than 20 characters, same limit applies to VO names.
- Only LimeSurvey versions prior to 6.x:
    - Use __sub__ attribute instead of __eduPersonUniqueID__ for local user identifier (this is also a LimeSurvey limitation of username length).

## Compatibility

The following LimeSurvey versions are required (for API functions needed):
- =3.x: 3.26.0 and above
- \>4.x: 4.5.0 and above (5.x, 6.x)

## Logging

Plugin events are logged in the default LimeSurvey _plugin.log_ located in the _/tmp/runtime_ folder with the prefix _[plugin.Helmholtz AAI]_.

## Installation

1. Go to LimeSurvey /plugins directory (the path assumed for base installation is _/var/www/html/limesurvey_).
    ```
    cd /var/www/html/limesurvey/plugins
    ```
2. Clone the plugin repository in the current directory (__\<branch\>__ e.g. _main_ or _develop_).
    ```
    git clone --branch <branch> https://gitlab.hzdr.de/hifis/cloud/si/limesurvey/AuthHelmholtzAAI.git .
    ```
3. Change to the created plugin directory __AuthHelmholtzAAI__.
    ```
    cd AuthHelmholtzAAI
    ```
4. Install necessary dependencies via composer.
    ```
    composer install
    ```
5. Activate and configure the plugin in the LimeSurvey admin backend at the plugin manager.

## Update

1. Go to plugin directory (the path assumed for base installation is _/var/www/html/limesurvey_).
    ```
    cd /var/www/html/limesurvey/plugins/AuthHelmholtzAAI
    ```
2. Update the plugin using Git (__\<branch\>__ e.g. _master_ or _develop_).
    ```
    git pull origin <branch>
    ```
3. Deactivate the plugin in the LimeSurvey admin backend at the plugin manager and activate it again.

## Configuration

The plugin ships with reasonable default settings and descriptive configuration labels with sample values.

## Credits

Thanks to Michael Jett and other contributers for providing the [OpenID Connect Client](https://github.com/jumbojett/OpenID-Connect-PHP)!
