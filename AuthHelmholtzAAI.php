<?php

/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software consists of voluntary contributions made by many individuals
 * and is licensed under the MIT license. For more information, see
 * <http://www.hifis.net>.
 */

/**
 * Cloud-SI-LimeSurvey - A LimeSurvey authentication plugin that uses OpenID Connect
 *                       with authorization based on the eduPerson attribute schema.
 *
 * @license    http://www.opensource.org/licenses/mit-license.html  MIT License
 * @author     Florian Döring <f.doering@dkfz-heidelberg.de>
 * @copyright  2024 German Cancer Research Center (Deutsches Krebsforschungszentrum, DKFZ)
 * @link       https://codebase.helmholtz.cloud/hifis/cloud/si/services/limesurvey/AuthHelmholtzAAI
 */
class AuthHelmholtzAAI extends AuthPluginBase
{
    // Pretty useful for debugging:
    // traceVar($myTraceVar); throw new \Exception('Handbrake');

    protected $storage = 'DbStorage';
    static protected $name = 'Helmholtz AAI';   // Display name of the plugin
    static protected $description = 'Authentication and Auhorisation to Helmholtz AAI via OpenID Connect'; // Description of the plugin

    // Here we define our plugin settings
    protected $settings = array(
        'oidc_issuer' => array(
            'type' => 'string',
            'label' => 'OIDC issuer URL (e.g. https://login-dev.helmholtz.de/oauth2/, defaults to none)',
            'default' => '',
        ),
        'oidc_client_id' => array(
            'type' => 'string',
            'label' => 'OIDC client ID (e.g. client123, defaults to none)',
            'default' => '',
        ),
        'oidc_client_secret' => array(
            'type' => 'string',
            'label' => 'OIDC client secret (e.g. mysecret, defaults to none)',
            'default' => '',
        ),
        'oidc_redirect_uri' => array(
            'type' => 'string',
            'label' => 'OIDC redirect URI (e.g. https://mysurveys.net/index.php/admin/authentication/sa/login (v3) or https://mysurveys.net/index.php?r=admin/authentication/sa/login (v4+), defaults to none/autodetect)',
            'default' => '',
        ),
        'oidc_http_proxy' => array(
            'type' => 'string',
            'label' => 'The proxy to use for OIDC protocol communication, if required (e.g. http://proxy:80/, defaults to none)',
            'default' => '',
        ),
        'authuser_id' => array(
            'type' => 'string',
            'label' => 'Attribute of user ID representing the local identifier (defaults to sub)',
            'default' => 'sub',
        ),
        'authuser_entitlements' => array(
            'type' => 'string',
            'label' => 'Attribute of Entitlements (defaults to eduperson_entitlement)',
            'default' => 'eduperson_entitlement',
        ),
        'authuser_display_name' => array(
            'type' => 'string',
            'label' => 'Attribute of user display name (defaults to display_name)',
            'default' => 'display_name',
        ),
        'authuser_email' => array(
            'type' => 'string',
            'label' => 'Attribute of user email address (defaults to email)',
            'default' => 'email',
        ),
        'authuser_email_verified' => array(
            'type' => 'string',
            'label' => 'Attribute of user email address verification indicator (defaults to email_verified)',
            'default' => 'email_verified',
        ),
        'authuser_email_dummy' => array(
            'type' => 'string',
            'label' => 'Dummy value of user email address if verification indicator is false (e.g. no-reply@dkfz.de, defaults to none)',
            'default' => '',
        ),
        'login_form_html' => array(
            'type' => 'html',
            'label' => 'Descriptive information to display below Authentication methods',
            'default' => '',
        ),
        'login_form_links' => array(
            'type' => 'json',
            'label' => 'Hyperlinks to add to the login form (e.g. [{"Acceptable use policy": "http://service.ex/aup.html"}, {"Imprint": "http://service.ex/imprint.html"}, {"Data privacy protection": "http://service.ex/privacy.html"}], defaults to none)',
            'editorOptions' => array('mode' => 'tree'),
            'default' => '',
        ),
        'required_entitlements' => array(
            'type' => 'json',
            'label' => 'Entitlements required to login, is evaluated as a logical OR, ANY of these entitlements must be present, is MANDATORY (e.g. ["urn:geant:h-df.de:group:DKFZ_LimeSurvey_Dev1", "urn:geant:h-df.de:group:DKFZ_LimeSurvey_Dev2"], defaults to none)',
            'editorOptions' => array('mode' => 'tree'),
            'default' => '',
        ),
        'deprovision_user' => array(
            'type' => 'checkbox',
            'label' => 'Deprovision users on failed login attempt by moving to a dedicated group for unauthorized users (defaults to false)',
            'default' => false,
        ),
        'deprovisioned_groupname' => array(
            'type' => 'string',
            'label' => 'Name for group of deprovisioned users (read-only, defaults to "Unauthorized Users")',
            'default' => 'Unauthorized Users',
            'htmlOptions' => array(
                'readonly' => true,
            ),
        ),
        'is_default' => array(
            'type' => 'checkbox',
            'label' => 'Check to make default authentication method (defaults to false)',
            'default' => false,
        ),
        'auto_create_user' => array(
            'type' => 'checkbox',
            'label' => 'Auto-create user if it does not exists (defaults to false)',
            'default' => false,
        ),
        'auto_assign_themes' => array(
            'type' => 'checkbox',
            'label' => 'Auto-assign themes to users based on user\'s group (defaults to false)',
            'default' => false,
        ),
        'permission_create_survey' => array(
            'type' => 'checkbox',
            'label' => 'Set permission for auto-created users to create surveys (defaults to false)',
            'default' => false,
        ),
        'allow_remote_control_auth' => array(
            'type' => 'checkbox',
            'label' => 'Allow remote control auth via access token (defaults to false)',
            'default' => false,
        ),
        'revoke_oauth_token' => array(
            'type' => 'checkbox',
            'label' => 'Revoke OAuth token on logout (defaults to false)',
            'default' => false,
        ),
    );

    /**
     *  Subscribe to plugin events
     */
    public function init()
    {
        $this->subscribe('beforeActivate');
        $this->subscribe('newLoginForm');
        $this->subscribe('beforeLogin');
        $this->subscribe('afterLoginFormSubmit');
        $this->subscribe('newUserSession');
        if ($this->get('deprovision_user')) {
            $this->subscribe('afterFailedLoginAttempt');
        }
        if ($this->get('allow_remote_control_auth')) {
            $this->subscribe('remoteControlLogin');
        }
        if ($this->get('revoke_oauth_token')) {
            $this->subscribe('beforeLogout');
        }
    }

    /**
     * Activate or not
     */
    public function beforeActivate()
    {
        if (!@include(__DIR__ . "/vendor/autoload.php")) {
            $this->getEvent()->set('message', gT("Requirements have not been installed, please run 'composer install' from the plugin directory."));
            $this->getEvent()->set('success', false);
        }

        
        // Error with 6.x
        /** if (version_compare(Yii::app()->getConfig('versionnumber'),"4.0.0","<") and version_compare(Yii::app()->getConfig('versionnumber'),"3.26.0","<")) {
             $this->getEvent()->set('message', gT("Only for LimeSurvey 3 version 3.26.0 and later."));
             $this->getEvent()->set('success', false);
         } elseif (version_compare(Yii::app()->getConfig('versionnumber'),"4.0.0","=>") and version_compare(Yii::app()->getConfig('versionnumber'),"4.5.0","<")) {
             $this->getEvent()->set('message', gT("Only for LimeSurvey 4 version 4.5.0 and later."));
             $this->getEvent()->set('success', false);
         }
         */
    }

    /** Object \Jumbojett\OpenIDConnectClient */
    private $oidcClient = null;

    // The user identifier to include in logs, is set later
    private $logUser = '';

    /**
     * Provides an instance of our OIDC lib
     * @return OpenIDConnectClient
     */
    private function getOidcClient()
    {
        if ($this->oidcClient == null) {
            // Load OIDC lib via composer
            require_once(__DIR__ . "/vendor/autoload.php");
            // Set OIDC lib parameters from plugin settings
            $oidcIssuer = $this->get('oidc_issuer');
            $oidcClientId = $this->get('oidc_client_id');
            $oidcClientSecret = $this->get('oidc_client_secret');
            $oidcRedirectUri = $this->get('oidc_redirect_uri');
            $oidcHttpProxy = $this->get('oidc_http_proxy');
            $this->oidcClient = new Jumbojett\OpenIDConnectClient($oidcIssuer, $oidcClientId, $oidcClientSecret);
            // If redirect URI is defined in plugin settings, set it to OIDC lib
            if (!empty($oidcRedirectUri)) {
                $this->oidcClient->setRedirectURL($oidcRedirectUri);
            }
            // If proxy is defined in plugin settings, set it to OIDC lib
            if (!empty($oidcHttpProxy)) {
                $this->oidcClient->setHttpProxy($oidcHttpProxy);
            }
        }
        return $this->oidcClient;
    }

    /**
     *  Extends the login form for the plugin
     */
    public function newLoginForm()
    {
        // Add HTML and hyperlinks to the login form, if configured
        if ($this->get('login_form_links')) {
            $loginFormLinks = json_decode($this->get('login_form_links'), true);
            $loginFormLinksHtml = '';
            foreach ($loginFormLinks as $loginFormLinkKey => $loginFormLinkValue) {
                $delimiter = ($loginFormLinkKey == count($loginFormLinks) - 1) ? '' : '&nbsp;|&nbsp;';
                $loginFormLinksHtml .= '<a href="' .
                    $loginFormLinkValue[key($loginFormLinkValue)] .
                    '" target="_blank">' .
                    key($loginFormLinkValue) . '</a>' . $delimiter;
            }
            $this->getEvent()->setContent($this)
                ->addContent(CHtml::tag(
                    'span',
                    array(),
                    "<br />". $this->get('login_form_html') .
                    "<br /><p>" . $loginFormLinksHtml . "</p>"
                ));
        } else {
            // Must be present to display selectbox of login methods
            $this->getEvent()->setContent($this)->addContent(CHtml::tag('span', array()));
        }
    }

    /**
     *  After click on 'Log in'
     */
    public function afterLoginFormSubmit()
    {
        // If we have an access token, continue with newUserSession()
        if (!empty(Yii::App()->session['access_token'])) {
            return;
        }

        // Set the OIDC scopes
        $oidcScopes = $this->getOidcScopes();

        // Save OIDC Scopes to Yii/PHP session to get the claims later
        Yii::App()->session['oidc_scope'] = $oidcScopes;

        $oidcClient = $this->getOidcClient();   // Get an OIDC client instance

        // Add the OIDC Scopes
        foreach ($oidcScopes as $oidcScope) {
            $oidcClient->addScope($oidcScope);
        }
        $this->log("Initiating OIDC authentication.", CLogger::LEVEL_INFO);
        $oidcClient->authenticate();    // Authenticate user via OIDC
    }

    /**
     *  Before login form is shown
     */
    public function beforeLogin()
    {
        $request = Yii::app()->request; // Get the client's get and post data
        // If we have an error on callback, log and fail auth
        if ($request->getParam('error')) {
            $this->log("OIDC authentication returned an error: " . $request->getParam('error'), CLogger::LEVEL_ERROR);
            $this->setAuthFailure(self::ERROR_AUTH_METHOD_INVALID, gT('Authentication method returned an error.'));
            return;
        }
        // If we have an authorization code on callback, get the access token
        if ($request->getParam('code')) {
            $oidcClient = $this->getOidcClient();   // Get an OIDC client instance
            if ($oidcClient->authenticate()) {  // Takes the authorization code and gets the access token
                $this->log("Successful OIDC authentication.", CLogger::LEVEL_INFO);
                Yii::App()->session['access_token'] = $oidcClient->getAccessToken();    // Save the access token in the PHP/Yii Session
                $this->setAuthPlugin(); // This plugin handles authentication, halt further execution of auth plugins
            } else {
                $this->log("Authentication method returned an error: " . $request->getParam('error'), CLogger::LEVEL_ERROR);
                $this->setAuthFailure(self::ERROR_AUTH_METHOD_INVALID, gT('Authentication method returned an error.'));
            }
        }

        // If this is configured to be the default login method
        if ($this->get('is_default')) {
            $this->getEvent()->set('default', get_class($this));    // We set is as default
        }
    }

    /**
     *  Deprovision existing user
     */
    private function deprovisionUser($dUid)
    {
        $this->log("Deprovisioning user " . $this->logUser, CLogger::LEVEL_INFO);
        // Create group for deprovisioned users
        $deprovisionedGroupName = $this->get('deprovisioned_groupname');
        // Get an array of all user groups
        $existingUserGroups = $this->api->getUserGroups();
        // Create group for deprovisoned users if it does not already exist
        if (array_search($deprovisionedGroupName, array_column($existingUserGroups, 'name')) === false) {
            if ($this->api->addUserGroup($deprovisionedGroupName, 'Users no longer authorised go here.')) {
                $this->log("Created group " . $deprovisionedGroupName, CLogger::LEVEL_INFO);
            }
        }
        // Remove user from local group(s) and add user to deprovisioned user group
        foreach ($existingUserGroups as $userGroupKey => $userGroupValue) {
            if (strcmp($userGroupValue['name'], $deprovisionedGroupName) !== 0) {
                if ($this->api->removeUserInGroup($userGroupValue['ugid'], $dUid)) {
                    $this->log("Removed user " . $this->logUser . " from group id " . $userGroupValue['ugid'], CLogger::LEVEL_INFO);
                }
            } else {
                if ($this->api->addUserInGroup($userGroupValue['ugid'], $dUid)) {
                    $this->log("Added user " . $this->logUser . " to group " . $deprovisionedGroupName, CLogger::LEVEL_INFO);
                }
            }
        }
    }

    /**
     *  Create groups
     */
    private function createUserGroups($userGroups)
    {
        // Iterate over permitted groups of which user is member of
        foreach ($userGroups as $userGroup) {
            // Get an array of all user groups
            $existingUserGroups = $this->api->getUserGroups();
            // Create local group if it does not already exist
            if (array_search($userGroup, array_column($existingUserGroups, 'name')) === false) {
                if ($this->api->addUserGroup($userGroup, $userGroup)) {
                    $this->log("Created group " . $userGroup . " for user " . $this->logUser, CLogger::LEVEL_INFO);
                } else {
                    $this->log("FATAL_ERROR - createUserGroups", CLogger::LEVEL_ERROR);
                    throw new CHttpException(401, 'Wrong credentials for LimeSurvey administration.');
                }
            }
        }
    }

    /**
     *  Create user
     */
    private function createUser($cUser)
    {
        $this->log("Creating new user " . $this->logUser, CLogger::LEVEL_INFO);
        $password = date('YmdHis') . rand(0, 1000);    // Generate random password
        $oUser = new User;
        $oUser->users_name = $cUser['user_id'];
        $oUser->full_name = $cUser['full_name'];
        $oUser->email = $cUser['email'];
        $oUser->parent_id = 1;
        $oUser->created = date('Y-m-d H:i:s');
        $oUser->modified = date('Y-m-d H:i:s');
        $oUser->password = password_hash($password, PASSWORD_DEFAULT);
        if (!$oUser->save()) {
            // User could not be created, which should never happen
            $this->log("FATAL_ERROR - createUser " . $this->logUser, CLogger::LEVEL_ERROR);
            throw new CHttpException(401, 'ERROR_USERNAME_INVALID.');
        }
        // Set user permission to create survey
        if ($cUser['permission_create_survey']) {
            $permission = new Permission;
            $permission->entity_id = 0;
            $permission->entity = 'global';
            $permission->uid = $oUser->uid;
            $permission->permission = 'surveys';
            $permission->create_p = 1;
            $permission->read_p = 0;
            $permission->update_p = 0;
            $permission->delete_p = 0;
            $permission->import_p = 0;
            $permission->export_p = 0;
            $permission->save();
        }
        return $oUser;
    }

    /**
     *  Reflect user groups
     */
    private function reflectUserInGroups($rUid, $userGroups)
    {
        // Add user to own local groups
        $allGroups = $this->api->getUserGroups();
        foreach ($allGroups as $allGroupsKey => $allGroupsValue) {
            foreach ($userGroups as $userGroup) {
                if (strcmp($allGroupsValue['name'], $userGroup) == 0) {
                    if (empty($this->api->getUserInGroup($allGroupsValue['ugid'], $rUid))) {
                        if ($this->api->addUserInGroup($allGroupsValue['ugid'], $rUid)) {
                            $this->log("Added internal user id " . $rUid . " to internal group id " . $allGroupsValue['ugid'] . ' for user ' . $this->logUser, CLogger::LEVEL_INFO);
                        } else {
                            $this->log("FATAL_ERROR - reflectUserInGroups", CLogger::LEVEL_ERROR);
                            throw new CHttpException(401, 'Wrong credentials for LimeSurvey administration.');
                        }
                    }
                }
            }
        }
        // Remove user from other groups
        $removalUserGroups = $allGroups;
        foreach ($userGroups as $userGroup) {
            foreach ($removalUserGroups as $userGroupKey => $userGroupValue) {
                if (strcmp($userGroupValue['name'], $userGroup) == 0) {
                    unset($removalUserGroups[$userGroupKey]);
                }
            }
        }
        foreach ($removalUserGroups as $removalUserGroupKey => $removalUserGroupValue) {
            if (!empty($this->api->getUserInGroup($removalUserGroupValue['ugid'], $rUid))) {
                if ($this->api->removeUserInGroup($removalUserGroupValue['ugid'], $rUid)) {
                    $this->log("Removed internal user id " . $rUid . " from internal group id " . $removalUserGroupValue['ugid'] . ' for user ' . $this->logUser, CLogger::LEVEL_INFO);
                } else {
                    $this->log("FATAL_ERROR - reflectUserInGroups", CLogger::LEVEL_ERROR);
                    throw new CHttpException(401, 'Wrong credentials for LimeSurvey administration.');
                }
            }
        }
    }

    /**
     *  Update user
     */
    private function updateUser($oUser, $uUser)
    {
        if (strcmp($oUser->full_name, $uUser['full_name']) !== 0 or strcmp($oUser->email,  $uUser['email']) !== 0) {
            $this->log("Updating user info for user " . $oUser->uid, CLogger::LEVEL_INFO);
            $oUser->full_name =  $uUser['full_name'];
            $oUser->email =  $uUser['email'];
            $oUser->modified = date('Y-m-d H:i:s');
            if (!$oUser->save()) {
                $this->log("Updating user info failed for user " . $oUser->uid, CLogger::LEVEL_ERROR);
            }
        }
    }

    /**
     *  Assign themes
     */
    private function assignThemes($oUser, $userGroups)
    {
        $templateRights = array();
        $tRights = Permission::model()->findAllByAttributes(array('uid' => $oUser->uid, 'entity'=>'template'));
        foreach ($tRights as $tRight) {
            $templateRights[$tRight["permission"]] = $tRight["read_p"];
        }

        $templates = array_keys($this->api->getTemplateList());
        foreach ($userGroups as $userGroup) {
            $userGroup = str_replace(':', '-', $userGroup);
            foreach ($templates as $template) {
                if (substr_compare($template, $userGroup, 0, strlen($userGroup)) === 0) {
                    $templateRights[$template] = "1";
                }
            }
        }
        $this->log("assignThemes(" . $oUser->users_name  . "): " . json_encode($templateRights), CLogger::LEVEL_INFO);

        foreach ($templateRights as $key => $value) {
            $oPermission = Permission::model()->findByAttributes(array('permission' => $key, 'uid' => $oUser->uid, 'entity'=>'template'));
            if (empty($oPermission)) {
                $oPermission = new Permission;
                $oPermission->uid = $oUser->uid;
                $oPermission->permission = $key;
                $oPermission->entity = 'template';
                $oPermission->entity_id = 0;
            }
            $oPermission->read_p = $value;
            $uresult = $oPermission->save();
            if ($uresult !== true) {
                $this->log("assignThemes(" . $oUser->users_name  . "): ERROR", CLogger::LEVEL_ERROR);
            }
        }
    }

    /**
    * Get OIDC Scopes
    */
    private function getOidcScopes()
    {
        // Set an array for the OIDC scopes
        $oidcScopes = [$this->get('authuser_id')];   // User ID is always required to be set as local identifier

        // Set other OIDC scopes depending on plugin settings
        if (!empty($this->get('authuser_entitlements'))) {
            array_push($oidcScopes, $this->get('authuser_entitlements'));
        }
        if (!empty($this->get('authuser_display_name'))) {
            array_push($oidcScopes, $this->get('authuser_display_name'));
        }
        if (!empty($this->get('authuser_email'))) {
            array_push($oidcScopes, $this->get('authuser_email'));
        }
        if (!empty($this->get('authuser_email_verified'))) {
            array_push($oidcScopes, $this->get('authuser_email_verified'));
        }

        // single logout with modfied version of jumbojett OIDC lib
        if ($this->get('revoke_oauth_token')) {
            array_push($oidcScopes, 'single-logout');
        }

        return $oidcScopes;
    }

    /**
     *  Here we authorize the user
     */
    public function newUserSession()
    {

        // Do nothing if user is not coming from our plugin
        $identity = $this->getEvent()->get('identity');
        if ($identity->plugin != 'AuthHelmholtzAAI') {
            return;
        }

        $oidcClient = $this->getOidcClient();   // Get an OIDC client instance

        // If RemoteControlAPI authentication enabled in plugin settings and username 'access_token' was provided
        if ($this->get('allow_remote_control_auth') and strcmp($this->getUsername(), 'access_token') == 0) {
            $this->log("Authentication attempt for RemoteControl2 via access_token.", CLogger::LEVEL_INFO);
            $accessToken = $this->getPassword();    // Password is the access token from api auth call
            $oidcClient->setAccessToken($accessToken);   // Set provided access toke to OIDC client instance
            // Set the OIDC Scopes
            // because we don't have it here from previous afterLoginFormSubmit()
            // as this is not triggered at RemoteControlAPI authentication
            $oidcScopes = $this->getOidcScopes();
             // Set OIDC Scopes in the Yii/PHP Session
            Yii::App()->session['oidc_scope'] = $oidcScopes;
        }

        // If access token was not set via RemoteControlAPI authentication, it must be in the Yii/PHP session
        if (empty($oidcClient->getAccessToken())) {
            $accessToken = Yii::App()->session['access_token'];    // Get the access token from Yii/PHP session
            $oidcClient->setAccessToken($accessToken);  // Set provided access toke to OIDC client instance
        }

        // Get and Set OIDC Claims by scope that was set
        $oidcScopes = Yii::App()->session['oidc_scope'];
        $oidcUserInfo = [];
        foreach ($oidcScopes as $oidcScope) {
            $oidcUserInfo[$oidcScope] =  $oidcClient->requestUserInfo($oidcScope);
        }

        // Construct array for authenticated user from OIDC claims
        $authUser = array(
            'user_id' => isset($oidcUserInfo[$this->get('authuser_id')]) ? $oidcUserInfo[$this->get('authuser_id')] : null,
            'full_name' => isset($oidcUserInfo[$this->get('authuser_display_name')]) ? $oidcUserInfo[$this->get('authuser_display_name')] : null,
            'email' => (isset($oidcUserInfo[$this->get('authuser_email_verified')]) ? (bool) $oidcUserInfo[$this->get('authuser_email_verified')] : false) ? (isset($oidcUserInfo[$this->get('authuser_email')]) ? $oidcUserInfo[$this->get('authuser_email')] : null) : $this->get('authuser_email_dummy'),
            'permission_create_survey' => $this->get('permission_create_survey'),
            'entitlements' => isset($oidcUserInfo[$this->get('authuser_entitlements')]) ? (array) $oidcUserInfo[$this->get('authuser_entitlements')] : null,
        );

        // Cut at the '#' from each entitlement because it's not needed and create new entitlements array
        // (urn:geant:h-df.de:group:DKFZ_LimeSurvey_Dev1#login-dev.helmholtz.de => urn:geant:h-df.de:group:DKFZ_LimeSurvey_Dev1)
        $userEntitlements = array_map(
            function($userEntitlement) { return explode('#', $userEntitlement)[0]; },
            $authUser['entitlements']
        );
        // Get the matches out of the required entitlements and the user's entitlements
        $requiredEntitlements = json_decode($this->get('required_entitlements'), true);
        $matchingEntitlements = array_intersect($requiredEntitlements, $userEntitlements);
        /**
        * Local user group name is right from ':group:'
        * (urn:geant:h-df.de:group:DKFZ_LimeSurvey_Dev1 => DKFZ_LimeSurvey_Dev1)
        */
        $authUser['local_groups'] = array_map(
            function($matchingEntitlement) { return substr(stristr($matchingEntitlement, ":group:"), 7); },
            $matchingEntitlements
        );

        if (empty($authUser['user_id'])) {
            // Unique identifier is missing, which should never happen
            $this->log("FATAL_ERROR - User ID not set", CLogger::LEVEL_ERROR);
            throw new CHttpException(401, 'Wrong credentials for LimeSurvey administration.');
        } else {
            // Set the user ID string to include into logs
            $this->logUser = $authUser['user_id'];
            // Set the user name requesting login
            $this->setUsername($authUser['user_id']);
            $entitlementPass = empty(json_decode($this->get('required_entitlements'))) ? false : !empty($matchingEntitlements);
            if (empty(json_decode($this->get('required_entitlements')))) {
                $this->log("Configuration of required_entitlements is empty!",CLogger::LEVEL_ERROR);
            }
            // If entitlements not met
            if (!$entitlementPass) {
                // Deny access
                $this->log("Access denied for user " . $this->logUser, CLogger::LEVEL_ERROR);
                $this->setAuthFailure(self::ERROR_UNKNOWN_IDENTITY, gT('Authorization failed.'));
            // All entitlements are met
            } else {
                $this->log("Access granted for user " . $this->logUser, CLogger::LEVEL_INFO);
                // If a user already exists by local identifier, get the object
                $oUser = $this->api->getUserByName($authUser['user_id']);
                // Auto-create user object if enabled or fail
                if (empty($oUser)) {
                    if ($this->get('auto_create_user')) {
                        $oUser = $this->createUser($authUser);
                    } else {
                        $this->log("Required object not found for user: " . $this->logUser, CLogger::LEVEL_ERROR);
                        $this->setAuthFailure(self::ERROR_UNKNOWN_IDENTITY, gT('Authorization failed.'));
                        return;
                    }
                }
                // Create user groups
                $this->createUserGroups($authUser['local_groups']);
                // Reflect group assignments of user
                $this->reflectUserInGroups($oUser->uid, $authUser['local_groups']);
                // Update user's full name and email if they were set (check by full_name)
                if (!empty($authUser['full_name'])) {
                    $this->updateUser($oUser, $authUser);
                }
                // Update theme permissions if enabled
                if ($this->get('auto_assign_themes')) {
                    $this->assignThemes($oUser, $authUser['local_groups']);
                }
                // Really should never happen - fail auth on last resort
                if (empty($oUser)) {
                    $this->log("FATAL_ERROR - User not found: " . $this->logUser, CLogger::LEVEL_ERROR);
                    throw new CHttpException(401, 'User not found.');
                } else {
                    // Set auth success
                    $this->log("AuthSuccess for user " . $this->logUser, CLogger::LEVEL_INFO);
                    $this->setAuthSuccess($oUser);
                }
            }
        }
    }

    /**
     *  After a failed login attempt
     */
    public function afterFailedLoginAttempt()
    {
        // If a user already exists by local identifier, get the object
        $oUser = $this->api->getUserByName($this->getUsername());
        // Deprovision existing user
        if (!empty($oUser)) {
            $this->deprovisionUser($oUser->uid);
        }
    }

    /**
     *  Before user session is destroyed on log out
     */
    public function beforeLogout()
    {
        // Get the access token from the Yii/PHP session
        $accessToken = Yii::App()->session['access_token'];
        if (!empty($accessToken)) {
            $oidcClient = $this->getOidcClient();   // Get an OIDC client instance
            $oidcClient->revokeToken($accessToken, 'access_token'); // Revoke access token and initate single-logout
        }
    }
}
